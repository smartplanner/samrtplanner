package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class An4Sem1 extends Activity
    {



        public FloatingActionButton b1;
        public Button td,tdav,pns,pi,cm,tv,rcc,take_me_home;
        public CheckBox homelocation;

        double lat = 46.755738;

        double lng = 23.592955;

        public void onBackPressed()
        {
            Intent intent = new Intent(An4Sem1.this,MainActivity.class);
            startActivity(intent);
        }

        public void homelocation( ) {
            homelocation = findViewById(R.id.changehome);
            homelocation.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View v) {
                                                    Intent trd = new Intent(An4Sem1.this, changehome.class);
                                                    startActivity(trd);

                                                }
                                            }

            );
        }
    public void pagtd( ){
        td = findViewById(R.id.button);
        td.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent trd= new Intent(An4Sem1.this, TransDate.class);
                                      startActivity(trd);

                                  }
                              }

        );
    }
    public void pagtdav( ){
        tdav = findViewById(R.id.button3);
        tdav.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent teh= new Intent(An4Sem1.this, TehniciDAV.class);
                                      startActivity(teh);

                                  }
                              }

        );
    }
    public void pagpns( ){
        pns = findViewById(R.id.button4);
        pns.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent pns= new Intent(An4Sem1.this, Pns.class);
                                      startActivity(pns);

                                  }
                              }

        );
    }
    public void pagtv( ){
        tv =  findViewById(R.id.button2);
        tv.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent tv= new Intent(An4Sem1.this, Televiziune.class);
                                      startActivity(tv);

                                  }
                              }

        );
    }
    public void pagcm( ){
        cm =  findViewById(R.id.button7);
        cm.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent cm= new Intent(An4Sem1.this, ComMobile.class);
                                      startActivity(cm);

                                  }
                              }

        );
    }
    public void pagrcc( ){
        rcc =  findViewById(R.id.button5);
        rcc.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      Intent rcc= new Intent(An4Sem1.this, RadioCC.class);
                                      startActivity(rcc);

                                  }
                              }

        );
    }
    public void pagpi( ){
        pi = findViewById(R.id.button6);
        pi.setOnClickListener(new View.OnClickListener() {
                                   public void onClick(View v) {
                                       Intent pi= new Intent(An4Sem1.this, ProtocoaleI.class);
                                       startActivity(pi);

                                   }
                               }

        );
    }

    public void inita(  )
    {
        b1= findViewById(R.id.floatingActionButton);

        b1.setOnClickListener(new View.OnClickListener()
                              {
                                  public void onClick(View v)
                                  {
                                      Intent orar=new Intent(An4Sem1.this,an4_sem1_g1.class);
                                      startActivity(orar);

                                  }
                              }

        );

    }

        public void take_me_home( ){
            take_me_home=  findViewById(R.id.take_me_home);
            take_me_home.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View v) {
                                            String format = "geo:0,0?q=" + lat + "," + lng + "(Strada Observatorului)";
                                            Uri uri = Uri.parse(format);
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                    }

            );
        }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_an4_sem1);

        TextView tx = findViewById(R.id.editText2);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx.setTypeface(custom_font);

        homelocation();
        inita();
        pagtd();
        pagtv();
        pagpns();
        pagrcc();
        pagcm();
        pagpi();
        pagtdav();
        take_me_home( );
    }

    }
