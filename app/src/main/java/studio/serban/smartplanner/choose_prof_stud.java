package studio.serban.smartplanner;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class choose_prof_stud extends Activity {

    public Button student,profesor;
    public MediaPlayer mMediaPlayer;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_prof_stud);
        init();
        init1();

        TextView tx = findViewById(R.id.bunvenit);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx.setTypeface(custom_font);

        TextView tx1 = findViewById(R.id.sau);

        Typeface custom_font1 = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx1.setTypeface(custom_font1);

        TextView tx2 = findViewById(R.id.profil);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx2.setTypeface(custom_font2);

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer = MediaPlayer.create(this, R.raw.hello);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.start();

    }

    public void init(){
        student=findViewById(R.id.student);
        student.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent a4s1=new Intent(choose_prof_stud.this,MainActivity.class);
                startActivity(a4s1);
            }
        });
    }

    public void init1(){
        profesor=findViewById(R.id.prof);
        profesor.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent a4s1=new Intent(choose_prof_stud.this,LoginActivity1.class);
                startActivity(a4s1);
            }
        });
    }
}
