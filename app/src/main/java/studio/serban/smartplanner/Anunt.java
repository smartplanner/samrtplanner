package studio.serban.smartplanner;



public class Anunt {
    private int id;
    private String anuntText;
    private String userName;


    public Anunt() {

    }

    public Anunt(int id,String anuntText,String userName) {
        this.id = id;

        this.anuntText=anuntText;
        this.userName = userName;


    }

    public Anunt(String anuntText,String userName) {
        this.anuntText=anuntText;
        this.userName = userName;
    }

    public void setID(int id) {
        this.id = id;
    }
    public int getID() {
        return this.id;
    }

    public void setAnuntText(String anuntText) {
        this.anuntText = anuntText;
    }

    public String getAnuntText() {
        return this.anuntText;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }
}

