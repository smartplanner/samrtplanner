package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ComMobile extends Activity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {


    public String anunturi_prof;
    public Button site,materiale,Mail;
    private GestureDetectorCompat mDetector;
    DatabaseHelper myDB;//*
    private SharedPreferences savednotes1;
    private EditText editText11;
    //private Anunt anunt;//*
    public static final String AnuntText = "Anunt";//*
    private  final Activity activity=ComMobile.this;//*
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_com_mobile);

        site_cm();
        email_cm();
        materiale_cm();

        TextView tx = findViewById(R.id.materie_CM);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx.setTypeface(custom_font);

        myDB = new DatabaseHelper(activity);//*
        editText11 = findViewById(R.id.notite1);
        savednotes1 = getSharedPreferences("notes1", MODE_PRIVATE);
        editText11.setText(savednotes1.getString(AnuntText, "Introduceti notite")); //modificattt

        mDetector = new GestureDetectorCompat(this, this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);

        final EditText one =  this.findViewById(R.id.notite1);
        one.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                makeTag1(editText11.getText().toString());//*
            }
        });
    }

    private String getAnunt(String userName)//*
    {

        List<Anunt> anunturi = myDB.getAllByUserName(userName);
        for(Anunt anunt : anunturi)
        {
            anunturi_prof= anunt.getAnuntText();
        }

        return anunturi_prof;
    }

    private void makeTag1(String tag1) {//*
        String anunt = getAnunt("Iulian.Benta@com.utcluj.ro");

        StringBuilder stringBuilder = new StringBuilder();
        if(anunt.length()!=0)
        stringBuilder.append("Anunt:\t");
        stringBuilder.append(anunt);
        stringBuilder.append("\n");
        stringBuilder.append(tag1);
        String finalString = stringBuilder.toString();

        SharedPreferences.Editor preferencesEditor = savednotes1.edit();
        preferencesEditor.putString(AnuntText, finalString);
        preferencesEditor.apply();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        return false;
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(ComMobile.this,An4Sem1.class);
        startActivity(intent);
    }

    @Override
    public void onLongPress(MotionEvent event) {

    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
    if(distanceY>0)//scroll down
    {
        Intent teh = new Intent(ComMobile.this, TransDate.class);
        startActivity(teh);
    }
    if(distanceY<0)//scroll up
    {
        Intent teh = new Intent(ComMobile.this, ProtocoaleI.class);
        startActivity(teh);
    }
        return true;
    }

    public void site_cm( ){
        site =  findViewById(R.id.Site);
        site.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse("https://helios.utcluj.ro/LAB/login.php?SID");
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(intent);
                                    }
                                }

        );
    }

    public void materiale_cm( ){
        materiale =findViewById(R.id.Materiale);
        materiale.setOnClickListener(new View.OnClickListener() {
                                         public void onClick(View v) {
                                             Uri uri = Uri.parse("https://drive.google.com/drive/folders/0B9s3dZgkqmNXbXJ5U21mN2FjOGM");
                                             Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                             startActivity(intent);
                                         }
                                     }

        );
    }

    public void email_cm( ){
        Mail =  findViewById(R.id.Mail);
        Mail.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse("https://www.yahoo.com");
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(intent);
                                    }
                                }

        );
    }

    @Override
    public void onShowPress(MotionEvent event) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {

        return false;
    }

}

