package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;


public class an4_sem1_g2 extends Activity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener{
    private GestureDetectorCompat mDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.an4_sem1_g2);

        mDetector = new GestureDetectorCompat(this,this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);

        RadioButton yourCheckBox = findViewById (R.id.TD);
        RadioButton yourCheckBox1 = findViewById (R.id.TV);
        RadioButton yourCheckBox2 = findViewById (R.id.TDAV);
        RadioButton yourCheckBox3 = findViewById (R.id.PNS);
        RadioButton yourCheckBox4 = findViewById (R.id.RCC);
        RadioButton yourCheckBox5 = findViewById (R.id.PI);
        RadioButton yourCheckBox6 = findViewById (R.id.CM);

        yourCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, TransDate.class);
                startActivity(teh);

            }
        });

        yourCheckBox1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, Televiziune.class);
                startActivity(teh);

            }
        });

        yourCheckBox2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, TehniciDAV.class);
                startActivity(teh);

            }
        });

        yourCheckBox3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, Pns.class);
                startActivity(teh);

            }
        });

        yourCheckBox4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, RadioCC.class);
                startActivity(teh);

            }
        });

        yourCheckBox5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, ProtocoaleI.class);
                startActivity(teh);

            }
        });

        yourCheckBox6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent teh= new Intent(an4_sem1_g2.this, ComMobile.class);
                startActivity(teh);

            }
        });

    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        if(distanceY>0)//scroll down
        {
            Intent teh = new Intent(an4_sem1_g2.this, an4_sem1_g3.class);
            startActivity(teh);
        }
        if(distanceY<0)//scroll up
        {
            Intent teh = new Intent(an4_sem1_g2.this, an4_sem1_g1.class);
            startActivity(teh);
        }
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);




        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }


    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(an4_sem1_g2.this,An4Sem1.class);
        startActivity(intent);
    }
}
