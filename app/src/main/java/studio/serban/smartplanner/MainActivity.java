package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    public Button show;

    public void init(){
        show=findViewById(R.id.show);
        show.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent a4s2=new Intent(MainActivity.this, An4Sem1.class);
                startActivity(a4s2);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      Toolbar toolbar =  findViewById(R.id.toolbar);

        TextView tx = findViewById(R.id.title);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx.setTypeface(custom_font);

        setSupportActionBar(toolbar);
               init();
        Button btn =  findViewById(R.id.An1);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myFancyMethod(v);
            }
        });
    }

    private void setSupportActionBar(Toolbar toolbar) {
    }

    public void myFancyMethod(View v) {

        Toast.makeText(MainActivity.this,"Anul 1 selectat!",Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(MainActivity.this,choose_prof_stud.class);
        startActivity(intent);
    }

    public void myFancyMethod1(View v) {

        Toast.makeText(MainActivity.this,"Anul 2 selectat!",Toast.LENGTH_SHORT).show();

    }
    public void myFancyMethod2(View v) {

        Toast.makeText(MainActivity.this,"Anul 3 selectat!",Toast.LENGTH_SHORT).show();

    }

    public void myFancyMethod3(View v) {

        Toast.makeText(MainActivity.this,"Anul 4 selectat!",Toast.LENGTH_SHORT).show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {





        return super.onOptionsItemSelected(item);
    }







}

