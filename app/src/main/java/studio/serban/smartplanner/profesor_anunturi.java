package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class profesor_anunturi extends Activity {
    DatabaseHelper myDB;
    private SharedPreferences savednotes1;
    private EditText editText11,user;
    private Button post;
    private  final Activity activity=profesor_anunturi.this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesor_anunturi);
        myDB = new DatabaseHelper(activity);

        user =  findViewById(R.id.username);
        editText11 =  findViewById(R.id.notiteGeneral);




        post = findViewById(R.id.posteaza);
        savednotes1 = getSharedPreferences("notesGeneral",MODE_PRIVATE);

        editText11.setText(savednotes1.getString("tag", "Introduceti notite")); //add this line


        post.setOnClickListener(saveButtonListener);

    }

    private void makeTag1(String tag1){

        SharedPreferences.Editor preferencesEditor = savednotes1.edit();
        preferencesEditor.putString("tag",tag1); //change this line to this
        preferencesEditor.apply();

    }

    public View.OnClickListener saveButtonListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            if(editText11.getText().length()>0){
                makeTag1(editText11.getText().toString());
                myDB.adaugaAnunt(new Anunt(editText11.getText().toString(),"Iulian.Benta@com.utcluj.ro"));
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText11.getWindowToken(),0);
                Intent intent = new Intent(profesor_anunturi.this,An4Sem1.class);
                       startActivity(intent);

                Toast.makeText(getApplicationContext(),"Anunt: "+editText11.getText().toString(),Toast.LENGTH_LONG).show();

            }
        }
    };


}
