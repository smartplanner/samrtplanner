package studio.serban.smartplanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;



public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Anunturi.db";
    public static final String TABLE_NAME = "mylist_data";
    public static final String ID = "ID";
    public static final String UserName="UserName";
    public static final String AnuntText="AnuntText";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " +
                TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY,"+ UserName + " Text,"  + AnuntText+ " Text" +")";

        db.execSQL(createTable);


    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP  TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    public void adaugaAnunt(Anunt anunt) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserName, anunt.getUserName());
        values.put(AnuntText, anunt.getAnuntText());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }
    public List<Anunt> getAllByUserName(String userName ) {

        String query = "Select * FROM " + TABLE_NAME + " WHERE " + UserName+ " =  \"" + userName + "\"";
        List<Anunt> anunturi;
        anunturi = new ArrayList<Anunt>();
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Anunt  anunt = new Anunt();
              //  anunt.setID(cursor.getInt(cursor.getColumnIndex("ID")));
             //   anunt.setUserName(cursor.getString(cursor.getColumnIndex("UserName")));
                anunt.setAnuntText(cursor.getString(cursor.getColumnIndex("AnuntText")));

                anunturi.add(anunt); }
            while (cursor.moveToNext());
        }
        return anunturi;
    }
    public List<Anunt> getAll( ) {

        //String query = "Select * FROM " + TABLE_NAME + " WHERE " + UserName+ " =  \"" + userName + "\"";
        List<Anunt> anunturi;
        anunturi = new ArrayList<Anunt>();
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("Select * FROM " + TABLE_NAME , null);
        if (cursor.moveToFirst()) {
            do {
                Anunt  anunt = new Anunt();
                //  anunt.setID(cursor.getInt(cursor.getColumnIndex("ID")));
                //   anunt.setUserName(cursor.getString(cursor.getColumnIndex("UserName")));
                anunt.setAnuntText(cursor.getString(cursor.getColumnIndex("AnuntText")));

                anunturi.add(anunt); }
            while (cursor.moveToNext());
        }
        return anunturi;
    }
}

