package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class changehome extends Activity {

    private EditText editText11,editText12;
    public Button post,conv;
    public String blockCharacterSet = "qwertyuiopasdfghjklzxcvbnm,/;'[]-=`!@#$%^&*)(";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changehome);

        editText11 =  findViewById(R.id.Latitudine);
        editText12 =  findViewById(R.id.Longitudine);
        post = findViewById(R.id.salveazacoordonatecasa);
        conv= findViewById(R.id.converter);

        editText11.setFilters(new InputFilter[] { filter });
        editText12.setFilters(new InputFilter[] { filter });



        converter();
        take_me_home_guest( );
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    public void onBackPressed()
    {
        Intent intent = new Intent(changehome.this,An4Sem1.class);
        startActivity(intent);
    }




    public void converter( ){
        conv =  findViewById(R.id.converter);
        conv.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse("https://www.latlong.net/");
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(intent);
                                    }
                                }

        );
    }

    public void take_me_home_guest( ){
        post=  findViewById(R.id.salveazacoordonatecasa);
        post.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {

                                                if(editText11.getText().toString().contains(".") && editText12.getText().toString().contains(".")) {
                                                    String format = "geo:0,0?q=" + editText11.getText() + "," + editText12.getText() + "(My Home)";
                                                    Uri uri = Uri.parse(format);
                                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                }
                                                else Toast.makeText(getApplicationContext(),"Nu ati respectat formatul LatLong",Toast.LENGTH_LONG).show();
                                            }
                                        }

        );
    }
}
