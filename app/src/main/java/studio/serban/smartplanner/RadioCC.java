package studio.serban.smartplanner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.view.GestureDetectorCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RadioCC extends Activity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {

    public Button site,materiale,Mail;

    private SharedPreferences savednotes1;
    private EditText editText11;

    private GestureDetectorCompat mDetector;

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(RadioCC.this,An4Sem1.class);
        startActivity(intent);
    }
    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_cc);

        site_rcc( );
        email_rcc( );
        materiale_rcc();

        TextView tx = findViewById(R.id.materie_RCC);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SaucerBB.ttf");

        tx.setTypeface(custom_font);


        editText11 =  findViewById(R.id.notite2);
        savednotes1 = getSharedPreferences("notes2",MODE_PRIVATE);

        editText11.setText(savednotes1.getString("tag", "Introduceti notite")); //add this line



        mDetector = new GestureDetectorCompat(this,this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);


        EditText one =  this.findViewById(R.id.notite2);
        one.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                makeTag1(editText11.getText().toString());



            }


        });

    }

    private void makeTag1(String tag1){

        SharedPreferences.Editor preferencesEditor = savednotes1.edit();
        preferencesEditor.putString("tag",tag1); //change this line to this
        preferencesEditor.apply();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);




        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {


        return false;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {

        return false;
    }

    @Override
    public void onLongPress(MotionEvent event) {

    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        if(distanceY>0)//scroll down
        {
            Intent teh= new Intent(RadioCC.this, ProtocoaleI.class);
            startActivity(teh);
        }
        if(distanceY<0)//scroll up
        {
            Intent teh= new Intent(RadioCC.this, Pns.class);
            startActivity(teh);
        }


        return true;
    }

    public void site_rcc( ){
        site =  findViewById(R.id.Site);
        site.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse("https://www.google.com");
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(intent);
                                    }
                                }

        );
    }

    public void materiale_rcc( ){
        materiale =  findViewById(R.id.Materiale);
        materiale.setOnClickListener(new View.OnClickListener() {
                                         public void onClick(View v) {
                                             Uri uri = Uri.parse("https://drive.google.com/drive/folders/0B9s3dZgkqmNXbUc1dEtZdFF0LUk");
                                             Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                             startActivity(intent);
                                         }
                                     }

        );
    }




    public void email_rcc( ){
        Mail = findViewById(R.id.Mail);
        Mail.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse("https://www.yahoo.com");
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(intent);
                                    }
                                }

        );
    }

    @Override
    public void onShowPress(MotionEvent event) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {

        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {

        return false;
    }

}

